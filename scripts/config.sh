# Define variables used in scripts
# --------------------------------
#
# Define some directories
DATADIR=data
RESDIR=res
LOGDIR=log
SCRIPTDIR=scripts
OUTDIR=out

# logfile: path of the pipeline log file
# infolog: File containing cutadapt and STAR selected information
# urlfile: path of the file containing sample URLs to download
# urlcont: URL of the contaminants database
logfile=$LOGDIR/pipeline.log
infolog=$LOGDIR/Log.out
urlfile=$DATADIR/urls
urlcont=https://bioinformatics.cnio.es/data/courses/decont/contaminants.fasta.gz

# cutadapt parameters
cutadaptMin=18
cutadaptAdapter=TGGAATTCTCGGGTGCCAAGG

# STAR parameters
starIndexNumFiles=9
starAlignNumFiles=6
starRunThreadN=4
starGenomeSAindexNbases=9
