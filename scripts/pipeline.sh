# Include config file
source $(dirname "$0")/config.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/config.sh' not found!" >&2
    exit 1
fi

# Include lib file
source $(dirname "$0")/lib.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/lib.sh' not found!" >&2
    exit 1
fi

# Store start time
startTime=$(date +%s)

# First of all, remove previous log file, if exists
rm -f $logfile

printlog "############################################################"
printlog "#"
printlog "# PIPELINE STARTED"
printlog "#"
printlog "############################################################"

printlog ""
printlog "Downloading the sequencing data files"
printlog "-------------------------------------"

# Download all the files specified in data/filenames

# ##################################################
# DOWNLOAD SAMPLE DATA FILES: LOOP VERSION
# #################################################
#
#for url in $(cat $urlfile)
#do
#    printlog "Downloading file $url"
#    bash $SCRIPTDIR/download.sh $url $DATADIR >> $logfile
#    retVal=$?
#    if [ $retVal -ne 0 ];
#    then
#        printerror "Error downloading file $1"
#	exit 1
#    fi
#done
# ##################################################
# DOWNLOAD SAMPLE DATA FILES: ONE LINE VERSION
# #################################################
#
# Check if any of the files listed in the urlfile exists
# The variable existingfiles will contain the URL of the existing files
# The variable notexistingfiles will contain the URL of non existing files, so we'll download them.

allfiles=$(cat $urlfile)
existingfiles=''
notexistingfiles=''
for file in $allfiles
do
    filename=$DATADIR/$(basename $file) # Extract the filename from the URL
    # Add the filename to the proper variable, depending on its existence
    if ! [ -f $filename ]; then notexistingfiles=$file" "$notexistingfiles; else existingfiles=$filename" "$existingfiles; fi
done

# Print existing files information in log file.
if [ -n "$existingfiles" ]; then printlog "Skipping existing files: $existingfiles"; fi

# Download not existing files
if [ -n "$notexistingfiles" ]; then
    printlog "Downloading files: $notexistingfiles"
    wget -q -P $DATADIR $notexistingfiles
    retVal=$?
    if [ $retVal -ne 0 ]; then
        printerror "Error downloading files $notexistingfiles"
        exit 1
    fi
    printlog "All sequencing data files downloaded successfully"
fi

printlog ""
printlog "Downloading the contaminants fasta file"
printlog "---------------------------------------"

# Check if contaminants uncompressed file exists
# First, we extract the filename from the URL. Then, we extract the extension (should be like 'gz'),
# finally, we check if the file without the extension exists.
filename=$(basename $urlcont)
extension="${filename##*.}"
uncompname=$(basename $filename .$extension)
if [ -f $RESDIR/$uncompname ]; then
    printlog "File $RESDIR/$uncompname exists. Skipping download."
else
    # Download the contaminants fasta file, and uncompress it
    printlog "Downloading file $urlcont"
    bash $SCRIPTDIR/download.sh $urlcont $RESDIR yes
    retVal=$?
    if [ $retVal -ne 0 ]
    then
	printerror "Error downloading file $urlcont"
	exit 1
    fi
    printlog "Contaminants file downloaded successfully"
fi

# Index the contaminants file
printlog ""
printlog "Indexing the contaminants file"
printlog "------------------------------"

bash $SCRIPTDIR/index.sh $RESDIR/contaminants.fasta $RESDIR/contaminants_idx
if [ "$?" -ne 0 ]; then
    printerror "Error executing STAR"
    exit 1
fi

# Merge the samples into a single file
printlog ""
printlog "Merging samples files"
printlog "---------------------"

# NOTE: SampleID are obtained from the sample filenames. 
# If sample filename is "C57BL_6NJ-12.5dpp.1.1s_sRNA.fastq.gz", then the sample ID will be "C57BL_6NJ"
# This behaviour can be changed by modifying the parameter "-d-" in the first cut sentence below
sampleIDs=$(ls $DATADIR/*fastq.gz 2>/dev/null | cut -d- -f1 | sort | uniq | sed "s:${DATADIR}/::")
for sid in $sampleIDs
do
    printlog "Merging sample '$sid'"
    bash $SCRIPTDIR/merge_fastqs.sh $DATADIR $OUTDIR/merged $sid
    if [ "$?" -ne 0 ]; then
        printerror "Error executing 'merge_fastqs.sh'"
        exit 1
    fi
done

# Run cutadapt for all merged files
printlog ""
printlog "Running cutadapt in merged files"
printlog "--------------------------------"

mkdir -p $OUTDIR/trimmed
mkdir -p $LOGDIR/cutadapt

for sid in $sampleIDs
do
    if [ -f $OUTDIR/trimmed/${sid}.trimmed.fastq.gz ]; then
        printlog "File '$OUTDIR/trimmed/${sid}.trimmed.fastq.gz' exists. Skipping cutadapt for sample '${sid}'."
    else
        printlog "Trimming Sample '$sid'"
        cutadapt -m $cutadaptMin -a $cutadaptAdapter --discard-untrimmed -o $OUTDIR/trimmed/${sid}.trimmed.fastq.gz $OUTDIR/merged/${sid}.fastq.gz > $LOGDIR/cutadapt/${sid}.log 2>/dev/null
        if [ "$?" -ne 0 ]; then
            printerror "Error executing cutadapt"
            exit 1
        fi
        # Checks if final log file exists, and then, append useful information to it
        if [ -f $LOGDIR/cutadapt/${sid}.log ]; then
            echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` cutadapt information for sample ID: '$sid'" >> $infolog
            cat $LOGDIR/cutadapt/${sid}.log | egrep "(^Reads with adapters)|(^Total basepairs)" >> $infolog
	    echo -e "---------------------------------------" >> $infolog
        fi
    fi
done

# Run STAR for all trimmed files
printlog ""
printlog "Running STAR in trimmed files"
printlog "-----------------------------"

for fname in $OUTDIR/trimmed/*.fastq.gz
do
    # you will need to obtain the sample ID from the filename. To obtain the sampleID from the path, we "reverse" 
    # the path, get the first element and the reverse it again
    sid=$(echo $fname | cut -d. -f1 | rev | cut -d/ -f1 | rev)

    # Check if the directory contains output files (it should contains $starAlignNumFiles files).
    numfiles=$(ls -1 $OUTDIR/star/$sid/ 2>/dev/null | wc -l)
    if [ "$numfiles" -eq "$starAlignNumFiles" ]; then
        printlog "Sample ID '$sid' already processed. Skipping it."
    else
        mkdir -p $OUTDIR/star/$sid
        printlog "Running STAR for sample '${sid}'"
        STAR --runThreadN $starRunThreadN --genomeDir $RESDIR/contaminants_idx --outReadsUnmapped Fastx --readFilesIn $fname --readFilesCommand gunzip -c --outFileNamePrefix $OUTDIR/star/$sid/ &>> $logfile
        if [ "$?" -ne 0 ]; then
            printerror "Error executing cutadapt"
            exit 1
        fi
        # Checks if final log file exists, and then, append useful information to it
        if [ -f $OUTDIR/star/$sid/Log.final.out ]; then
            echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` STAR information for sample ID: '$sid'" >> $infolog
            cat $OUTDIR/star/$sid/Log.final.out | grep "\ mapped" | grep "%" >> $infolog
	    echo -e "---------------------------------------" >> $infolog
        fi
    fi
done 

# Store finish time
finishTime=$(date +%s)

printlog ""
printlog "############################################################"
printlog "#"
printlog "# PIPELINE FINISHED: Check $infolog and $logfile for"
printlog "# more information."
printlog "#"
printlog "# Time elapsed: $(( (finishTime - startTime) )) seconds"
printlog "#"
printlog "############################################################"

