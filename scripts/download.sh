# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".

# Include config file
source $(dirname "$0")/config.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/config.sh' not found!" >&2
    exit 1
fi

# Include lib file
source $(dirname "$0")/lib.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/lib.sh' not found!" >&2
    exit 1
fi

# Function usage: prints usage information
usage () {
        echo "Usage: $0 <url> <destination directory> [uncompress (yes|no)]"
        echo "Download the file specified in the <url> parameter and places it in the <destination directory>."
        echo "Optionally, the third parameter indicates if the script should uncompresss the downloaded file."
}

# Check arguments and print help message if wrong number of them is given
if [ "$#" -lt 2 ] || [ "$#" -gt 3 ]; then
    usage
    exit 1
fi

# Create the output directory
mkdir -p $2

# Check if file exists. In that case, skip the download.
# NOTE: We only check for a file with the same name (as no MD5 or other comparision method exists).
filename=$(basename $1)
if [ -f $2/$filename ]; then
    printlog "File $2/$filename exists. Skipping download."
else
    # Download the file with wget
    # -P specifies the output directory
    # -q for quiet mode (no output if everything goes well)
    wget -q -P $2 $1
    retVal=$?
    if [ $retVal -ne 0 ]; then
        printerror "Error downloading file '$1'."
        exit 1
    fi
fi

# If uncompress option is specified, then uncompress file 
# -f is used to overwrite the file if it exists
if [ "$3" = "yes" ]; then
    printlog "Uncompressing file to '$2/$filename'."
    gunzip -fk $2/$filename
fi
