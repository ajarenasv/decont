# Scripts for Decontamination of small-RNA sequencing samples from mouse

## config.sh

Contains several variables needed to run the pipeline script:
<details>
<summary>Directories</summary>
<ul>
<li><b>DATADIR</b>: Directory for samples files (default <i>data</i>).</li>
<li><b>RESDIR</b>: Directory for resources like reference genoma (default <i>res</i>).</li>
<li><b>LOGDIR</b>: Directory for log files (default <i>log</i>).</li>
<li><b>SCRIPTDIR</b>: Directory for script files (default <i>scripts</i>).</li>
<li><b>OUTDIR</b>: Directory for output files (default <i>out</i>).</li>
</ul>
</details>

<details>
<summary>Input parameters</summary>
<ul>
<li><b>logfile</b>: Main log file for the pipeline.</li>
<li><b>infolog</b>: File containing information about cutadapt (reads with adapters and total basepairs) and STAR (percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci).</li>
<li><b>urlfile</b>: File containing samples URLs (one URL per line).</li>
<li><b>urlcont</b>: URL of the contaminants fasta file (gzipped).</li>
</ul>
</details>

<details>
<summary>cutadapt parameters</summary>
<ul>
<li><b>cutadaptMin</b>: Discard reads shorter than this number.</li>
<li><b>cutadaptAdapter</b>: 3' adapter to be removed from R2.</li>
</ul>
</details>

<details>
<summary>STAR parameters</summary>
<ul>
<li><b>starIndexNumFiles</b>: Number of files of an index (used to check if the indexing has been already done).</li>
<li><b>starAlignNumFiles</b>: Number of files used to check if the aligment has been already done.</li>
<li><b>starRunThreadN</b>: Value for <i>runThreadN</i> parameter.</li>
<li><b>starGenomeSAindexNbases</b>: Value for <i>genomeSAindexNbases</i> parameter.</li>
</ul>
</details>

## lib.sh

Contains general purpose functions:

- **printlog**: Print a formatted message (parameter `$1`) to a log file (`logfile` var).
- **printerror**: Print a formatted error message (parameter `$1`) to a log file (`logfile` var) and also to standard error (`stderr`).

## download.sh

Usage: `scripts/download.sh <url> <destination directory> [uncompress (yes|no)]`

Download the file specified in the `<url>` parameter and places it in the `<destination directory>`. If the file already exists, it doesn't download it again.  
Optionally, the third parameter indicates if the script should uncompress the downloaded file.

## index.sh

Usage: `index.sh <genome_file> <index_directory>`

This script indexes the genome file specified in the `genome_file` argument, creating the index in a directory specified by the `<index_directory>` argument.

If there are `starIndexNumFiles` (see `config.sh`) in the index directory, the script assumes that the indexing is already done and skips it.


## merge_fastqs.sh

Usage: `merge_fastqs.sh <sample_dir> <output_dir> <sample_id>`

Merge all files from a given sample (identified by `<sample_id>` and stored in `<sample_dir>` directory) into a single file, stored in `<output_dir>` directory.

The output file name will be `<output_dir>/<sample_id>.fastq.gz`.

If the output file already exists, the merging is skipped.

## pipeline.sh

Usage: `pipeline.sh`

Pipeline script for decontamination of small-RNA sequencing samples.

This pipeline execute the following actions:
- Download the sequencing data files (in a single wget instruction). The script checks for existing files.
- Download the contaminants database (calling `download.sh` script).
- Index the contaminants database (calling `index.sh` script).
- Merge the fastqs from the same sample into a single file  (calling `merge_fastqs.sh` script).
- Remove the adapters from the data (using `cutadapt`).
- Align the adapter-free reads to the contaminants genome, placing the non-aligned reads in FastQ format in output directory (using `star`)
- Create a log file with information on trimming and alignment results (`infolog` variable from `config.sh` file).
- Create a log file with information of the execution of the pipeline (`logfile` variable from `config.sh` file).

## clean.sh

Usage: `clean.sh`

Delete the date, log, res and out directories. Used for testing purposes.

> Use it with caution !!
