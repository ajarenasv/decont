# This script index the genome file specified in the first argument ($1),
# creating the index in a directory specified by the second argument ($2).

# Include config file
source $(dirname "$0")/config.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/config.sh' not found!" >&2
    exit 1
fi

# Include lib file
source $(dirname "$0")/lib.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/lib.sh' not found!" >&2
    exit 1
fi

# Prints usage information
usage () {
        echo "Usage: $0 <genome_file> <index_file>"
        echo "Index the genome file specified in the <genome_file> argument, creating the index in a directory specified by the <index_file> argument."
}

# Check arguments and print help message if wrong number of them is given
if [ "$#" -ne 2 ]; then
    usage
    exit 1
fi

# Create output directory
mkdir -p $2

# Check if the directory contains index files (if the genome is already indexed, it should be $starIndexNumFiles files).
numfiles=$(ls -1 $2 | wc -l)
if [ "$numfiles" -eq "$starIndexNumFiles" ]; then
    printlog "Directory $2 already contains $numfiles files. Skipping indexing the genome file."
    exit 0
fi

printlog "Running command: 'STAR --runThreadN $starRunThreadN --runMode genomeGenerate --genomeDir $2 --genomeFastaFiles $1 --genomeSAindexNbases $starGenomeSAindexNbases'"
STAR --runThreadN $starRunThreadN --runMode genomeGenerate --genomeDir $2 --genomeFastaFiles $1 --genomeSAindexNbases $starGenomeSAindexNbases &>> $logfile 
