# This script should merge all files from a given sample (the sample id is provided in the third argument ($3))
# into a single file, which should be stored in the output directory specified by the second argument ($2).
# The directory containing the samples is indicated by the first argument ($1).

# Include config file
source $(dirname "$0")/config.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/config.sh' not found!" >&2
    exit 1
fi

# Include lib file
source $(dirname "$0")/lib.sh
if [ "$?" -ne 0 ]; then
    echo "File '$(dirname "$0")/lib.sh' not found!" >&2
    exit 1
fi

# Prints usage information
usage () {
        echo "Usage: $0 <sample_dir> <output_dir> <sample_id>"
	echo "Merge all files from a given sample (identified by <sample_id> and stored in <sample_dir> directory) into a single file, stored in <output_dir> directory."
}

# Check arguments and print help message if wrong number of them is given
if [ "$#" -ne 3 ]; then
    usage
    exit 1
fi

# Create output directory
mkdir -p $2

if [ -f "$2/$3.fastq.gz" ]; then
    printlog "File '$2/$3.fastq.gz' already exists. Skipping merging."
    exit 0
fi

samples=$(ls $1/$3*.fastq.gz 2>/dev/null | sort)
if [ -z "$samples" ]; then printerror "No files found for sample $1/$3!"; exit 1; fi

printlog "Merging sample files:\n$samples"
cat $samples > $2/$3.fastq.gz
