# A small set of useful functions
# -------------------------------

printlog() {
    echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1" >> $logfile
}

printerror () {
   echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1" >> $logfile
   echo -e "`date +'[%Y/%m/%d %H:%M:%S]'` $0: $1" >&2
}
